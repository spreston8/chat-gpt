import siteMetadata from '@data/siteMetadata';
import { PageSeo } from '@components/SEO';
import Form from '@components/Form';
import { useEffect, useState } from 'react';

type EventHandler = {
  key: (e: React.KeyboardEvent) => void;
  preventDefault: () => void;
};

export default function Home() {
  const [codeInput, setCodeInput] = useState('');
  const [loading, setLoading] = useState(false);
  const [openaiRes, setOpenaiRes] = useState('');

  useEffect(() => {
    const keyDownHandler: any = async (event: EventHandler) => {
      if (event.key === ('Enter' as unknown)) {
        event.preventDefault();
        await onSubmit(codeInput);
      }
    };
    document.addEventListener('keydown', keyDownHandler);
    return () => {
      document.removeEventListener('keydown', keyDownHandler);
    };
  }, [codeInput]);

  async function onSubmit(_prompt: string) {
    setLoading(true);
    const response = await fetch('/api/generate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ prompt: _prompt }),
    });
    const data = await response.json();
    setOpenaiRes(data.result);
    setLoading(false);
  }

  return (
    <>
      <PageSeo
        title={siteMetadata.title}
        description={siteMetadata.description}
        url={siteMetadata.siteUrl}
        previewPath=""
      />

      <div className="text-center pt-10">
        <p>
          {siteMetadata.headerTitle} is an online AI bot providing knowledge and
          support to users 24/7. It is built using OpenAI's Chat-GPT and free
          for everyone. This tool will answer questions ranging across various
          topics from ancient history to debugging code to telling jokes.
        </p>
      </div>

      <div className="pt-10 px-20">
        <Form
          setValue={setCodeInput}
          className="bg-gray-200 dark:bg-gray-400 w-full border-none px-4 py-3 rounded-md outline-none focus:ring-0 text-sm"
          placeholder="Enter your prompt here"
        />
      </div>

      <div className="flex justify-center">
        <button
          className="text-white py-3 px-12 rounded-md drop-shadow-md mt-5 bg-blue-500 hover:bg-blue-600 disabled:bg-gray-400"
          type="submit"
          disabled={codeInput ? false : true}
          onClick={async () => await onSubmit(codeInput)}
        >
          Submit
        </button>
      </div>

      <div className="pt-10 text-center">
        {loading ? <>Generating response...</> : <>{openaiRes}</>}
      </div>
    </>
  );
}
