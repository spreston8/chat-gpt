import '../css/tailwind.css';
import { useEffect } from 'react';

import { DefaultSeo } from 'next-seo';
import Head from 'next/head.js';

import { SEO } from '@components/SEO';
import LayoutWrapper from '@components/LayoutWrapper';

import useRouterContext from '@lib/RouterContext';
import * as ga from '@lib/gtag';

function _App({ Component, pageProps }) {
  const router = useRouterContext();

  useEffect(() => {
    const handleRouteChange = (url: string) => {
      ga.pageview(url);
    };

    // When the component is mounted, subscribe to router changes
    // and log those page views
    router.events.on('routeChangeComplete', handleRouteChange);

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);

  return (
    <>
      <Head>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
      </Head>
      <DefaultSeo {...SEO} />
      <LayoutWrapper>
        <Component {...pageProps} />
      </LayoutWrapper>
    </>
  );
}

export default _App;
