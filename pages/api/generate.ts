import { Configuration, OpenAIApi } from 'openai';

const configuration = new Configuration({
  apiKey: process.env.NEXT_PUBLIC_OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

export default async function (req, res) {
  const completion = await openai.createCompletion({
    model: 'text-davinci-003',
    prompt: req.body.prompt,
    temperature: 0.6,
    max_tokens: 2048
  });
  console.log(completion)
  console.log(completion.data.choices[0].text)
  res.status(200).json({ result: completion.data.choices[0].text });
}
