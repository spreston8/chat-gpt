export default function SectionContainer({children}) {
  return <div className="overflow-x-hidden max-w-lg xs:max-w-xl md:max-w-3xl lg:max-w-5xl xl:max-w-7xl px-4 mx-auto md:px-6 xl:px-0">{children}</div>;
}
