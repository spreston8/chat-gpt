const siteMetadata = {
  title: 'Easy Helper',
  author: 'Stephen Preston',
  headerTitle: 'Easy Helper',
  description: 'OpenAI Chat-GPT bot',
  socialBanner: '',
  siteUrl: 'https://gitlab.com/spreston8/chat-gpt',
  siteRepo: 'https://gitlab.com/spreston8/chat-gpt',
  email: 'stevepreston747@gmail.com',
  gitlab: 'https://gitlab.com/spreston8',
  twitter: 'https://twitter.com',
  linkedin: 'https://www.linkedin.com/in/spreston8/',
};

export default siteMetadata;
